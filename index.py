from datetime import datetime, timedelta

time_format =  '%H:%M'

day_time_start_time = datetime.strptime("06:00", time_format)
day_time_end = datetime.strptime("22:00", time_format)
night_time_start_time = datetime.strptime("22:00", time_format)
night_time_end = datetime.strptime("06:00", time_format)

next_day = datetime.strptime("00:00", time_format)
day_end = datetime.strptime("23:59", time_format)
start_time2 = None

time_entry1 = input("Enter a start time in hh:mm format: \n")
start_time = datetime.strptime(time_entry1, time_format)

time_entry2 = input("Enter a end time in hh:mm format: \n")
end_time = datetime.strptime(time_entry2, time_format)

seq1 =[]
seqday = 0
seqnight = 0

if start_time > end_time:
    start_time2 = next_day
    end_time2 = end_time
    end_time = day_end


def calc_time(start, end, day, night):
    while start < end:
        if start > day_time_start_time and start < day_time_end:
            day +=1
        else:
            night +=1
        
        seq1.append(start_time)
        start += timedelta(minutes=15)
    return day, night

seqday, seqnight = calc_time(start_time, end_time, seqday, seqnight)
if start_time2 is not None:
    seqday, seqnight = calc_time(start_time2, end_time2, seqday, seqnight)

seqday = seqday/4
seqnight = seqnight/4
#print( [t.strftime("%H:%M") for t in seq1] )

print("dayshift time:")
print(seqday)
print("nightshift time:")
print(seqnight)
