# Day- and night shift time calculator
## V1.00
## how to install?

- git clone or download script
- install python3
## how to run?
- cd into folder
- python ./index.py
- Enter a start and end time in 24 hour format (HH:MM)
## settings
can be changed in script revaluating variables:

- day_time_start_time - dayshift start, default "06:00"
- day_time_end - dayshift end, default "22:00"
- night_time_start_time - nightshift start, default "22:00"
- night_time_end - nightshift end, default "06:00"